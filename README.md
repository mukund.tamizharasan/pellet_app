# Pellet Detection App


- Create a new Conda environment and activate it.
- Run `pip install -r requirements.txt` to install all requirements.
- Run `streamlit run strmlit_appwts.py` to start the application.
- Open `localhost:8501` to interact with the application.


