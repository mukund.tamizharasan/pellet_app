from model_tiny import *
from data import *
from keras.callbacks import TensorBoard
import time
#os.environ["CUDA_VISIBLE_DEVICES"] = "0"

#m=load_model('logt_0408/ep054.h5')
#m.summary()
model = unet(pretrained_weights='ep057.h5')

testGene = testGenerator("image/",as_gray=False)
start=time.time()
results = model.predict_generator(testGene,2, verbose=1)
print("Prediction speed:",time.time()-start)
saveResult("pred/", results)
