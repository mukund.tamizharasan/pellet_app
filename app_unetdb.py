import streamlit as st
import numpy as np
import math
import matplotlib
matplotlib.use('Agg')
from flask import *
import numpy as np
import cv2
import tensorflow as tf
import time
import os
from PIL import Image
import click
from collections import Counter
import matplotlib.pyplot as plt
import altair as alt
import pandas as pd

from model_tiny import *
from deblur_unet import *


with open("style.css") as f:
    st.markdown('<style>{}</style>'.format(f.read()), unsafe_allow_html=True)

st.set_option('deprecation.showfileUploaderEncoding', False)
weight_path = 'generator.h5'
input_dir = 'static/crp/'
output_dir = 'static/deblur/'

reg = [[350, 606, 606, 862], [350, 606, 862, 1118], [606, 862, 606, 862], [
    606, 862, 862, 1118], [350, 606, 1118, 1374], [606, 862, 1118, 1374]]

# Interactive Streamlit elements, like these sliders, return their value.
# This gives you an extremely simple interaction model.
st.title("Pellet Detection")
st.sidebar.title("Settings")
fr= st.sidebar.slider("Output video duration in seconds", 1, 20, 2, 1)
fps= st.sidebar.slider("Output video FPS", 1,30,1,1)


# Non-interactive elements return a placeholder to their location
# in the app. Here we're storing progress_bar to update it later.
progress_bar = st.sidebar.progress(0)

# These two elements will be filled in later, so we create a placeholder
# for them using st.empty()
frame_text = st.sidebar.empty()

det=st.sidebar.empty()

files=st.file_uploader("Upload video:")
show_file = st.empty()
while not files:
    show_file.info("Please upload a video file!")
content = files.getvalue()
vid_sub=open("static/subvideo.mp4","wb")
vid_sub.write(content)
show_file.empty()


text1=st.empty()
image1 = st.empty()
text2=st.empty()
image2 = st.empty()
text3=st.empty()
image3 = st.empty()
crop_text=st.empty()
crops=st.empty()

cap = cv2.VideoCapture("static/subvideo.mp4")
ret, frame = cap.read()
video_fps = cap.get(cv2.CAP_PROP_FPS)
cap.release()
Width = frame.shape[1]
Height = frame.shape[0]


vid_info="<h2>Input video details<h2><p>FPS: "+str(math.ceil(video_fps))+"</p>"\
    "<p>Video resolution: "+str(Height)+"x"+str(Width)+"</p>"
        
show_file.markdown(vid_info,unsafe_allow_html=True)

@st.cache
def run_app():
    idx = 1
    cap = cv2.VideoCapture("static/subvideo.mp4")
    inp = cv2.VideoWriter("static/orgvid.mp4",
                            cv2.VideoWriter_fourcc(*'H264'), fps, (Width, Height))
    outp = cv2.VideoWriter("static/predvid.mp4",
                            cv2.VideoWriter_fourcc(*'H264'), fps, (Width, Height))
    graph = cv2.VideoWriter("static/graph.mp4",
                            cv2.VideoWriter_fourcc(*'H264'), fps, (640,480))

    g = deblur_unet(pretrained_weights='deblur_unet.h5')
    model = unet(pretrained_weights='ep057.h5')
    sh_ar = []

    while(cap.isOpened()):

        cr_ar = []
        ret, frame = cap.read()
        if ret is not True or idx == (fr*fps+1):
            cap.release()
            outp.release()
            graph.release()
            inp.release()
            print("Exiting program!")
            break

        orgfr=frame.copy()

        progress_bar.progress(int((idx/(fr*fps))*100))
        frame_text.text("Frame "+str(idx)+"/"+str(fr*fps))
        # fr_sel.slider("Frame selector", 0,idx,1,1)

        crp=np.zeros((6,256,256,3))
        for i in range(6):
            crp[i,:,:,:]=frame[reg[i][0]:reg[i][1],reg[i][2]:reg[i][3],:]
        
        
        
        db_pre=crp/255
        start = time.time()
        deblur=g.predict(db_pre)
        temp=time.time()-start
        print(" Deblur Prediction speed:",temp)
        db_dep=deblur * 255
        db_dep=db_dep.astype('uint8')
        crp1=db_dep.copy()

        db_dep=db_dep / 255.
        start = time.time()
        seg=model.predict(db_dep)
        temp1=time.time()-start
        print("UNet Prediction speed:",temp1)
        #print('Segmentation shape',seg.shape)

        num_pel=0
        pdict={'s1':0,'s2':0,'s3':0,'s4':0,'s5':0}
        for i in range(1, 7):
            db = db_dep[i-1,:,:,:]
            db=db*255
            db=db.astype(np.uint8)
            gray = seg[i-1,:,:,0]
            gray=gray*255
            gray = gray.astype(np.uint8)            
            ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV) 
            thresh = thresh.astype(np.uint8)
            num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(
                thresh, 8, cv2.CV_32S)
            cr_fl=0    
            for j in stats:
                if j[4]>=100 and j[4] < 200:
                    pdict['s1']+=1
                if j[4]>=200 and j[4] < 300:
                    pdict['s2']+=1
                if j[4]>=300 and j[4] < 400:
                    pdict['s3']+=1
                if j[4]>=400 and j[4] < 500:
                    pdict['s4']+=1
                if j[4] >=500 and j[4] <=1000:
                    pdict['s5']+=1

                if j[4] >= 0 and j[4] <= 1000:
                    sh_ar.append(j[4])
                    num_pel+=1
                    cv2.rectangle(db, (j[0], j[1]),
                                    (j[0]+j[2], j[1]+j[3]), (0, 0, 255), 1)
                if j[4] >= 800 and j[4] <= 1000:
                    cr_fl=1
                    cv2.rectangle(crp1[i-1,:,:,:], (j[0], j[1]),
                                    (j[0]+j[2], j[1]+j[3]), (0, 0, 255), 1)
            if cr_fl==1:
                cr_ar.append(crp1[i-1,:,:,:])                      
            frame[reg[i-1][0]:reg[i-1][1], reg[i-1][2]:reg[i-1][3], :] = db
        
        det_text="<h2>Inference times<h2><p>Deblur inference speed: <span class='highlight blue bold'>"+str(float("{:.2f}".format(temp)))+"s</span></p>"\
            "<p>UNet inference speed: <span class='highlight blue bold'>"+str(float("{:.2f}".format(temp1)))+"s</span></p>"\
        "<h2>Detections<h2><p>Pellets detected: <span class='highlight red bold'>"+str(num_pel)+"</span></p>"\
            "<p>Pellet area 100-200: <span class='highlight red bold'>"+str(pdict['s1'])+"</span></p>"\
                "<p>Pellet area 200-300: <span class='highlight red bold'>"+str(pdict['s2'])+"</span></p>"\
                    "<p>Pellet area 300-400: <span class='highlight red bold'>"+str(pdict['s3'])+"</span></p>"\
                        "<p>Pellet area 400-500: <span class='highlight red bold'>"+str(pdict['s4'])+"</span></p>"\
                            "<p>Pellet area 500-1000: <span class='highlight red bold'>"+str(pdict['s5'])+"</span></p>"

        det.markdown(det_text,unsafe_allow_html=True)

        print("Number of pellets:",num_pel)
        cv2.imwrite("static/fr"+str(idx)+".jpg",frame)
        outp.write(frame)
        inp.write(orgfr)

        counter = Counter(sh_ar)
        xrang=list(counter.keys())
        yrang=[]
        crang=list(np.ones(len(xrang)))
        for z in xrang:
            yrang.append(counter.get(z,0))

        plt.figure(num=None,figsize=(8, 6), dpi=80)
        plt.scatter(xrang,yrang)
        plt.ylabel('No. of pellets')
        plt.xlabel('Area')
        plt.savefig('foo.jpg')
        plt.close()
        graph.write(cv2.imread('foo.jpg'))


        idx += 1

        text1.subheader("Original Image")
        text2.subheader("Predicted Image")
        text3.subheader("Scatter Plot")
        image1.image(orgfr, use_column_width=True,channels='BGR')
        image2.image(frame, use_column_width=True,channels='BGR')
        image3.image(cv2.imread('foo.jpg'), use_column_width=True,channels='BGR')
        crop_text.subheader("Crops with pellet area >800")
        crops.image(cr_ar)

run_app()

def disp():
    text1.subheader("Original Video")
    text2.subheader("Predicted Video")
    text3.subheader("Dynamic Scatter Plot")
    image1.video(open('static/orgvid.mp4', 'rb').read())
    image2.video(open('static/predvid.mp4', 'rb').read())
    image3.video(open('static/graph.mp4', 'rb').read())

    fr_sel=st.sidebar.slider("Frame selector", 1,fps*fr,1,1)
    crop_text.subheader("Frame #"+str(fr_sel))
    crops.image(cv2.imread("static/fr"+str(fr_sel)+".jpg"),use_column_width=True,channels='BGR')

disp()
# We clear elements by calling empty on them.
progress_bar.empty()
frame_text.empty()
det.empty()


# Streamlit widgets automatically run the script from top to bottom. Since
# this button is not connected to any other logic, it just causes a plain
# rerun.
st.button("Re-run")

